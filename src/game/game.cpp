#include "game.h"

namespace pong
{
	void init()
	{
			menu::init();
			gameplay::init();
			instructions::init();
			winner_screen::init();
			credits::init();
			game_selection::init();
			color_change::init();
			controls_config::init();
			InitWindow(screenWidth, screenHeight, "Pong Game");
			SetTargetFPS(60);
	}
	void inputs()
	{
		switch (state)
		{
		case MenuScreen:

			menu::inputs(state);
			break;

		case GameScreen:

			gameplay::inputs(player1, player2, vsIA, ballPosition, pause, state);
			break;

		case InstructionsScreen:

			instructions::inputs(state);
			break;

		case WinScreen:

			winner_screen::inputs(player1, player2, state);
			break;

		case CreditsScreen:

			credits::inputs(state);
			break;

		case GameSelectionScreen:

			game_selection::inputs(state, vsIA);
			break;

		case ColorChangeScreen:

			color_change::inputs(player1, player2, PadColorList, selectedColor, colorPointer, color_change::turn, state);
			break;

		case ControlsConfigScreen:

			controls_config::inputs(state, player1, player2);
			break;
		}
	}
	void update()
	{
		switch (state)
		{
		case GameScreen:

			gameplay::update(ballPosition, ballSpeed, ballRadius, player1, player2, collision, state, pause);
			break;
		}
	}
	void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);

		switch (state)
		{
		case MenuScreen:

			menu::draw();
			break;

		case GameScreen:

			gameplay::draw(player1, player2, ballPosition, ballRadius, pause);
			break;

		case InstructionsScreen:

			instructions::draw(player1, player2);
			break;

		case WinScreen:

			winner_screen::draw(player1, player2);
			break;

		case CreditsScreen:

			credits::draw();
			break;

		case GameSelectionScreen:

			game_selection::draw();
			break;

		case ColorChangeScreen:

			color_change::draw(player1, player2, colorPointer, PadColorList);
			break;

		case ControlsConfigScreen:

			controls_config::draw(player1, player2);
			break;
		}
		EndDrawing();
	}
	void deinit()
	{
		color_change::deinit();
		controls_config::deinit();
		credits::deinit();
		game_selection::deinit();
		gameplay::deinit();
		instructions::deinit();
		menu::deinit();
		winner_screen::deinit();
		CloseWindow();
	}
	void run()
	{
		pong::init();

		while (!WindowShouldClose())
		{
			pong::inputs();
			pong::update();
			pong::draw();
		}
		pong::deinit();
	}
}