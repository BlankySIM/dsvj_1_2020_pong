#ifndef GAME_H
#define GAME_H
#include "init\init.h"
#include "scenes\menu\menu.h"
#include "scenes\instrucctions\instructions.h"
#include "scenes\winner_screen\winner_screen.h"
#include "scenes\gameplay\gameplay.h"
#include "scenes\credits\credits.h"
#include "scenes\game_selection\game_selection.h"
#include "scenes\color_change\color_change.h"
#include "scenes\controls_configurations\controls_config.h"

namespace pong
{
	void init();
	void inputs();
	void update();
	void draw();
	void deinit();
	void run();
}
#endif

