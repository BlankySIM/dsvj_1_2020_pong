#ifndef CONTROLSCONFIG_H
#define CONTROLSCONFIG_H
#include "raylib.h"
#include "scenes\screens_states.h"
#include "player\player.h"

namespace pong
{
	using namespace player;

	namespace controls_config
	{
		void init();
		void keyChange(PLAYER &player, PLAYER theOtherPlayer);
		void inputs(GameState &state, PLAYER &player1, PLAYER &player2);
		void draw(PLAYER &player1, PLAYER &player2);
		void deinit();
	}
}
#endif