#include "controls_config.h"

namespace pong
{
	namespace controls_config
	{
		enum KeyChange
		{
			FirstKey,
			SecondKey,
			ShowKeys
		};
		enum ConfigMenus
		{
			ShowOptions,
			P1ControlChange,
			P2ControlChange
		};

		ConfigMenus currentMenu = ShowOptions;
		KeyChange currentKey = FirstKey;
		const int defaultKeyValue = -1;
		const int asciiFirstMinusc = 96;
		const int asciiFirstMayusc = 32;
		const int firstAllowedKey = 48;
		const int lastAllowedKey = 90;
		int key = defaultKeyValue;

		void init()
		{

		}
		void keyChange(PLAYER &player, PLAYER theOtherPlayer)
		{
			switch (currentKey)
			{
			case FirstKey:

				key = GetKeyPressed();
				if (key > asciiFirstMinusc)
				{
					key -= asciiFirstMayusc;
				}

				if (key >= firstAllowedKey && key <= lastAllowedKey && key != theOtherPlayer.upKey && key != theOtherPlayer.downKey && key != KEY_P)
				{
					player.upKey = key;
					currentKey = SecondKey;
				}
				break;

			case SecondKey:

				key = GetKeyPressed();
				if (key > asciiFirstMinusc)
				{
					key -= asciiFirstMayusc;
				}

				if (key >= firstAllowedKey && key <= lastAllowedKey && key != theOtherPlayer.downKey && key != theOtherPlayer.upKey && key != player.upKey && key != KEY_P)
				{
					player.downKey = key;
					currentKey = ShowKeys;
				}
				break;

			case ShowKeys:

				if (IsKeyPressed(KEY_ENTER))
				{
					currentKey = FirstKey;
					currentMenu = ShowOptions;
				}
				break;
			}
		}
		void inputs(GameState &state, PLAYER &player1, PLAYER &player2)
		{
			switch (currentMenu)
			{
			case ShowOptions:

				if (IsKeyPressed(KEY_M))
				{
					state = InstructionsScreen;
				}
				else if (IsKeyPressed(KEY_ONE))
				{
					currentMenu = P1ControlChange;
				}
				else if (IsKeyPressed(KEY_TWO))
				{
					currentMenu = P2ControlChange;
				}
				break;

			case P1ControlChange:

				keyChange(player1, player2);
				break;

			case P2ControlChange:

				keyChange(player2, player1);
				break;
			}

		}
		void draw(PLAYER &player1, PLAYER &player2)
		{
			switch (currentMenu)
			{
			case ShowOptions:

				DrawText("Configurations", middleScreenW-155, screenZero+30, maxFontSize, WHITE);
				DrawText("1->Change P1 controls", middleScreenW-170, middleScreenH-25, medFontSize, player1.padColor);
				DrawText("2->Change P2 controls", middleScreenW-178, middleScreenH+45, medFontSize, player2.padColor);
				DrawText("M -> Go back", screenWidth-195, screenHeight-30, minFontSize, WHITE);
				break;

			case P1ControlChange:

				switch (currentKey)
				{
				case FirstKey:

					DrawText("Press Up key", middleScreenW-110, middleScreenH-55, medFontSize, player1.padColor);
					break;

				case SecondKey:

					DrawText("Up -> ", middleScreenW-60, middleScreenH-55, medFontSize, player1.padColor);
					DrawText(FormatText("%c", static_cast<char>(player1.upKey)), middleScreenW+30, middleScreenH-55, medFontSize, player1.padColor);
					DrawText("Press down key", middleScreenW-125, middleScreenH+15, medFontSize, player1.padColor);
					break;

				case ShowKeys:

					DrawText("Up -> ", middleScreenW-60, middleScreenH-55, medFontSize, player1.padColor);
					DrawText(FormatText("%c", static_cast<char>(player1.upKey)), middleScreenW+30, middleScreenH-55, medFontSize, player1.padColor);
					DrawText("Down -> ", middleScreenW-75, middleScreenH+15, medFontSize, player1.padColor);
					DrawText(FormatText("%c", static_cast<char>(player1.downKey)), middleScreenW+50, middleScreenH+15, medFontSize, player1.padColor);
					DrawText("Press enter to continue", middleScreenW-177, middleScreenH+165, medFontSize, WHITE);
					break;
				}
				break;

			case P2ControlChange:

				switch (currentKey)
				{
				case FirstKey:

					DrawText("Press Up key", middleScreenW-110, middleScreenH-55, medFontSize, player2.padColor);
					break;

				case SecondKey:

					DrawText("Up -> ", middleScreenW-60, middleScreenH-55, medFontSize, player2.padColor);
					DrawText(FormatText("%c", static_cast<char>(player2.upKey)), middleScreenW+30, middleScreenH-55, medFontSize, player2.padColor);
					DrawText("Press down key", middleScreenW-125, middleScreenH+15, medFontSize, player2.padColor);
					break;

				case ShowKeys:

					DrawText("Up -> ", middleScreenW-60, middleScreenH-55, medFontSize, player2.padColor);
					DrawText(FormatText("%c", static_cast<char>(player2.upKey)), middleScreenW+30, middleScreenH-55, medFontSize, player2.padColor);
					DrawText("Down -> ", middleScreenW-75, middleScreenH+15, medFontSize, player2.padColor);
					DrawText(FormatText("%c", static_cast<char>(player2.downKey)), middleScreenW+50, middleScreenH+15, medFontSize, player2.padColor);
					DrawText("Press enter to continue", middleScreenW-177, middleScreenH+165, medFontSize, WHITE);
					break;
				}
				break;
			}
		}
		void deinit()
		{

		}
	}
}