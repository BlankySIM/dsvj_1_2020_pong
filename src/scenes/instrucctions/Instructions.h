#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H
#include"raylib.h"
#include "player\player.h"
#include "scenes\screens_states.h"

namespace pong
{
	using namespace player;

	namespace instructions
	{
		void init();
		void inputs(GameState &state);
		void draw(PLAYER &player1, PLAYER &player2);
		void deinit();
	}
}
#endif