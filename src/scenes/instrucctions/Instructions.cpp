#include"instructions.h"

namespace pong
{
	namespace instructions
	{
		void init()
		{

		}
		void inputs(GameState &state)
		{
			if (IsKeyPressed(KEY_M))
			{
				state = MenuScreen;
			}
			if (IsKeyPressed(KEY_C))
			{
				state = ControlsConfigScreen;
			}
		}
		void draw(PLAYER &player1, PLAYER &player2)
		{
			DrawText("How To Play", middleScreenW-125, screenZero+30, maxFontSize, WHITE);
			DrawRectangle(static_cast<int>(player1.posx), static_cast<int>(player1.posy), padW, padH, player1.padColor);
			DrawRectangle(static_cast<int>(player2.posx), static_cast<int>(player2.posy), padW, padH, player2.padColor);
			DrawText("Player 1 controls", screenZero+75, middleScreenH-105, medFontSize, player1.padColor);
			DrawText("Up -> ", screenZero+150, middleScreenH-35, medFontSize, player1.padColor);
			DrawText(FormatText("%c", static_cast<char>(player1.upKey)), screenZero+240, middleScreenH-35, medFontSize, player1.padColor);
			DrawText("Down -> ", screenZero+135, middleScreenH+65, medFontSize, player1.padColor);
			DrawText(FormatText("%c", static_cast<char>(player1.downKey)), screenZero+260, middleScreenH+65, medFontSize, player1.padColor);
			DrawText("Player 2 controls", screenWidth-350, middleScreenH-105, medFontSize, player2.padColor);
			DrawText("Up -> ", screenWidth-265, middleScreenH-35, medFontSize, player2.padColor);
			DrawText(FormatText("%c", static_cast<char>(player2.upKey)), screenWidth-175, middleScreenH-35, medFontSize, player2.padColor);
			DrawText("Down -> ", screenWidth-290, middleScreenH+65, medFontSize, player2.padColor);
			DrawText(FormatText("%c", static_cast<char>(player2.downKey)), screenWidth-165, middleScreenH+65, medFontSize, player2.padColor);
			DrawText("M -> Go to menu", screenWidth-200, screenHeight-30, minFontSize, WHITE);
			DrawText("C -> Change controls", screenZero+30, screenHeight-30, minFontSize, WHITE);
		}
		void deinit()
		{

		}
	}
}