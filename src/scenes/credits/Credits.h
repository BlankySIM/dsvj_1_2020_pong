#ifndef CREDITS_H
#define CREDITS_H
#include"raylib.h"
#include"scenes\screens_states.h"

namespace pong
{
	namespace credits
	{
		void init();
		void inputs(GameState &state);
		void draw();
		void deinit();
	}
}
#endif