#include"credits.h"

namespace pong
{
	namespace credits
	{
		void init()
		{

		}
		void inputs(GameState &state)
		{
			if (IsKeyPressed(KEY_M))
			{
				state = MenuScreen;
			}
		}
		void draw()
		{
			DrawText("Game dev: Blanco Juan Simon", middleScreenW-220, middleScreenH-145, medFontSize, PURPLE);
			DrawText("blancojuansimon@gmail.com", middleScreenW-200, middleScreenH-75, medFontSize, ORANGE);
			DrawText("Created with raylib library", middleScreenW-210, middleScreenH-5, medFontSize, RED);
			DrawText("www.raylib.com", middleScreenW-110, middleScreenH+65, medFontSize, WHITE);
			DrawText("M -> Go to menu", screenWidth-200, screenHeight-30, minFontSize, WHITE);
			DrawText("V1.0", screenZero+15, screenHeight-30, minFontSize, LIGHTGRAY);
		}
		void deinit()
		{

		}
	}
}