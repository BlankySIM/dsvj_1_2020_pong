#ifndef MENU_H
#define MENU_H
#include"raylib.h"
#include "scenes\screens_states.h"

namespace pong
{
	namespace menu
	{
		void init();
		void draw();
		void inputs(GameState &state);
		void deinit();
	}
}
#endif