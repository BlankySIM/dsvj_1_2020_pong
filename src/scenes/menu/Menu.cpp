#include "menu.h"

namespace pong
{
	namespace menu
	{
		void init()
		{

		}
		void draw()
		{
			DrawText("Pong Game", middleScreenW-110, screenZero+80, maxFontSize, WHITE);
			DrawText("1->Start Game", middleScreenW-110, middleScreenH-55, medFontSize, YELLOW);
			DrawText("2->Instructions", middleScreenW-125, middleScreenH+15, medFontSize, GREEN);
			DrawText("3->Credits", middleScreenW-85, 310, medFontSize, BLUE);
			DrawText("Press ESC to exit", screenWidth-200, screenHeight-30, minFontSize, WHITE);
			DrawText("V1.0", screenZero+15, screenHeight-30, minFontSize, LIGHTGRAY);
		}
		void inputs(GameState &state)
		{
			if (IsKeyPressed(KEY_ONE))
			{
				state = GameSelectionScreen;
			}
			else if (IsKeyPressed(KEY_TWO))
			{
				state = InstructionsScreen;
			}
			else if (IsKeyPressed(KEY_THREE))
			{
				state = CreditsScreen;
			}
		}
		void deinit()
		{

		}
	}
}