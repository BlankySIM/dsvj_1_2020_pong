#ifndef SCREENSTATES_H
#define SCREENSTATES_H

namespace pong
{
	const int screenZero = 0;
	const int minFontSize = 20;
	const int medFontSize = 30;
	const int maxFontSize = 40;
	const int screenWidth = 800;
	const int screenHeight = 450;
	const int middleScreenW = screenWidth / 2;
	const int middleScreenH = screenHeight / 2;

	enum GameState
	{
		MenuScreen,
		GameScreen,
		InstructionsScreen,
		WinScreen,
		CreditsScreen,
		GameSelectionScreen,
		ColorChangeScreen,
		ControlsConfigScreen
	};
}
#endif