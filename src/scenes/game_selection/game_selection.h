#ifndef GAMESELECTION_H
#define GAMESELECTION_H
#include "raylib.h"
#include "scenes\screens_states.h"

namespace pong
{
	namespace game_selection
	{
		void init();
		void inputs(GameState &state, bool &vsIA);
		void draw();
		void deinit();
	}
}
#endif