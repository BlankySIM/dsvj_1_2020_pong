#include "game_selection.h"

namespace pong
{
	namespace game_selection
	{
		void init()
		{

		}
		void inputs(GameState &state, bool &vsIA)
		{
			if (IsKeyPressed(KEY_M))
			{
				state = MenuScreen;
			}
			if (IsKeyPressed(KEY_ONE))
			{
				vsIA = false;
				state = ColorChangeScreen;
			}
			if (IsKeyPressed(KEY_TWO))
			{
				vsIA = true;
				state = ColorChangeScreen;
			}
		}
		void draw()
		{
			DrawText("Game Select", middleScreenW-125, screenZero+80, maxFontSize, WHITE);
			DrawText("1->P1 Vs. P2", middleScreenW-100, middleScreenH-55, medFontSize, PINK);
			DrawText("2->P1 Vs. PC IA", middleScreenW-125, middleScreenH+15, medFontSize, VIOLET);
			DrawText("M -> Go to menu", screenWidth-200, screenHeight-30, minFontSize, WHITE);
		}
		void deinit()
		{

		}
	}
}