#ifndef COLORCHANGE_H
#define COLORCHANGE_H
#include "raylib.h"
#include "scenes\screens_states.h"
#include "player\player.h"

namespace pong
{
	using namespace player;

	namespace color_change
	{
		enum ColorSelection
		{
			player1turn,
			player2turn,
			turnsDone
		};

		extern ColorSelection turn;

		void init();
		void inputs(PLAYER &player1, PLAYER &player2, Color padColorList[7], int &selectedColor, int &colorPointer, ColorSelection &turn, GameState &state);
		void draw(PLAYER &player1, PLAYER &player2, int colorPointer, Color padColorList[7]);
		void deinit();
	}
}
#endif