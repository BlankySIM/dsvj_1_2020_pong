#include "color_change.h"

namespace pong
{
	namespace color_change
	{
		const int selectedColorDefault = 7;
		const int squareSize = 50;
		ColorSelection turn = player1turn;

		void init()
		{

		}
		void inputs(PLAYER &player1, PLAYER &player2, Color padColorList[7], int &selectedColor, int &colorPointer, ColorSelection &turn, GameState &state)
		{
			switch (turn)
			{
			case player1turn:

				if (IsKeyPressed(KEY_K) && colorPointer < 6)
				{
					colorPointer++;
				}
				else if (IsKeyPressed(KEY_K))
				{
					colorPointer = 0;
				}

				if (IsKeyPressed(KEY_ENTER))
				{
					player1.padColor = padColorList[colorPointer];
					selectedColor = colorPointer;
					turn = player2turn;
				}

				break;

			case player2turn:

				if (IsKeyPressed(KEY_K) && colorPointer < 6)
				{
					colorPointer++;
				}
				else if (IsKeyPressed(KEY_K))
				{
					colorPointer = 0;
				}

				if (colorPointer == selectedColor)
				{
					colorPointer++;
				}
				if (colorPointer > 6)
				{
					colorPointer = 0;
				}

				if (IsKeyPressed(KEY_ENTER))
				{
					player2.padColor = padColorList[colorPointer];
					selectedColor = selectedColorDefault;
					turn = turnsDone;
				}

				break;

			case turnsDone:

				if (IsKeyPressed(KEY_ENTER))
				{
					state = GameScreen;
					turn = player1turn;
				}

				break;
			}
		}
		void draw(PLAYER &player1, PLAYER &player2, int colorPointer, Color padColorList[7])
		{
			switch (turn)
			{
			case player1turn:

				DrawText("Color select", middleScreenW-125, screenZero+30, maxFontSize, WHITE);
				DrawText("PLAYER 1 select a color", middleScreenW-190, screenZero+90, medFontSize, MAGENTA);
				DrawText("K-> Change color", middleScreenW-140, screenHeight-120, medFontSize, WHITE);
				DrawText("ENTER-> Confirm new color", middleScreenW-210, screenHeight-60, medFontSize, WHITE);
				DrawRectangle(middleScreenW-30, middleScreenH-25, squareSize, squareSize, padColorList[colorPointer]);

				break;

			case player2turn:

				DrawText("Color select", middleScreenW-125, screenZero+30, maxFontSize, WHITE);
				DrawText("PLAYER 2 select a color", middleScreenW-190, screenZero+90, medFontSize, GOLD);
				DrawText("K-> Change color", middleScreenW-140, screenHeight-120, medFontSize, WHITE);
				DrawText("ENTER-> Confirm new color", middleScreenW-210, screenHeight-60, medFontSize, WHITE);
				DrawRectangle(middleScreenW-30, middleScreenH-25, squareSize, squareSize, padColorList[colorPointer]);
				DrawText("P1 Ready", screenZero+50, middleScreenH-25, medFontSize, player1.padColor);

				break;

			case turnsDone:

				DrawText("Color select", middleScreenW-125, screenZero+30, maxFontSize, WHITE);
				DrawText("P1 Ready", screenZero+50, middleScreenH-25, medFontSize, player1.padColor);
				DrawText("P2 Ready", screenWidth-190, middleScreenH-25, medFontSize, player2.padColor);
				DrawText("Vs.", middleScreenW-35, middleScreenH-25, maxFontSize, WHITE);
				DrawText("Press ENTER  to start", middleScreenW-180, screenHeight-60, medFontSize, WHITE);

				break;
			}
		}
		void deinit()
		{

		}
	}
}