#include "winner_screen.h"

namespace pong
{
	namespace winner_screen
	{
		void init()
		{

		}
		void inputs(PLAYER &player1, PLAYER &player2, GameState &state)
		{
			if (IsKeyPressed(KEY_R))
			{
				state = GameScreen;
				player1.points = 0;
				player1.posx = p1DefaultX;
				player1.posy = p1DefaultY;
				player2.points = 0;
				player2.posx = p2DefaultX;
				player2.posy = p2DefaultY;
				player1.win = false;
				player2.win = false;
			}
			else if (IsKeyPressed(KEY_M))
			{
				state = MenuScreen;
				player1.points = 0;
				player1.posx = p1DefaultX;
				player1.posy = p1DefaultY;
				player2.points = 0;
				player2.posx = p2DefaultX;
				player2.posy = p2DefaultY;
				player1.win = false;
				player2.win = false;
				player1.roundCounter = 0;
				player2.roundCounter = 0;
			}
		}
		void draw(PLAYER &player1, PLAYER &player2)
		{
			DrawText(FormatText("%0i", player1.roundCounter), middleScreenW-125, screenZero+40, medFontSize, player1.padColor);
			DrawText(FormatText("%0i", player2.roundCounter), middleScreenW+90, screenZero+40, medFontSize, player2.padColor);

			if (player1.win)
			{
				DrawText("Player 1 Wins!", middleScreenW-105, middleScreenH-75, medFontSize, player1.padColor);
				DrawText("Press R to replay", middleScreenW-95, middleScreenH-5, minFontSize, WHITE);
				DrawText("Press M to go to the menu", middleScreenW-134, middleScreenH+45, minFontSize, WHITE);
				DrawText("Press ESC to exit", screenWidth-200, screenHeight-30, minFontSize, WHITE);
			}
			else if (player2.win)
			{
				DrawText("Player 2 Wins!", middleScreenW-105, middleScreenH-75, medFontSize, player2.padColor);
				DrawText("Press R to replay", middleScreenW-95, middleScreenH-5, minFontSize, WHITE);
				DrawText("Press M to go to the menu", middleScreenW-134, middleScreenH+45, minFontSize, WHITE);
				DrawText("Press ESC to exit", screenWidth-200, screenHeight-30, minFontSize, WHITE);
			}
		}
		void deinit()
		{

		}
	}
}