#ifndef WINNERSCREEN_H
#define WINNERSCEEN_H
#include "raylib.h"
#include "player\player.h"
#include "scenes\screens_states.h"

namespace pong
{
	using namespace player;

	namespace winner_screen
	{
		void init();
		void inputs(PLAYER &player1, PLAYER &player2, GameState &state);
		void draw(PLAYER &player1, PLAYER &player2);
		void deinit();
	}
}
#endif