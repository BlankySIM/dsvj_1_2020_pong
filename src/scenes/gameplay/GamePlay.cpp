#include "gameplay.h"

namespace pong
{
	namespace gameplay
	{
		const int WIN_POINTS = 3;
		const int PLAYER1 = 0;
		const int PLAYER2 = 1;
		const int minRandNum = 0;
		const int maxRandNum = 500;
		const int maxTimedRand = 100;
		const int powerUpTrigger = 50;
		const int IALessSped = 50;
		const int numPadRange = 272;
		const float defaultTextTime = 100;
		const int ObstacleMinY = screenZero+100;
		const int ObstacleMaxY = screenHeight-halfPadY-60;
		bool activeTimedPower = false;
		int poweredPlayer = PLAYER1;
		int timePoweredPlayer = PLAYER1;
		float textTimer = defaultTextTime;

		void init()
		{

		}
		void iA(PLAYER &player2, Vector2 ballposition)
		{
			if (ballposition.y < player2.posy + halfPadY && player2.posy >= 0)
			{
				player2.posy -= (player2.speed - IALessSped) * GetFrameTime();
			}
			else if (ballposition.y > player2.posy + halfPadY && player2.posy <= screenHeight - player2.padH)
			{
				player2.posy += (player2.speed - IALessSped) * GetFrameTime();
			}
		}
		void pauseInputs(bool &pause, GameState &state, PLAYER &player1, PLAYER &player2, Vector2 &ballPosition)
		{
			if (IsKeyPressed(KEY_P))
			{
				if (pause == true)
				{
					pause = false;
				}
				else if (pause == false)
				{
					pause = true;
				}
			}
			if (pause == true && IsKeyPressed(KEY_M))
			{
				state = MenuScreen;
				player1.points = 0;
				player1.posx = p1DefaultX;
				player1.posy = p1DefaultY;
				player2.points = 0;
				player2.posx = p2DefaultX;
				player2.posy = p2DefaultY;
				player1.win = false;
				player2.win = false;
				player1.roundCounter = 0;
				player2.roundCounter = 0;
				pause = false;
				ballPosition = { middleScreenW, middleScreenH };
				currentPowerUp = None;
				powerUps(player1);
				powerUps(player2);
				activePowerUp = false;
				powerTimer = 0;
			}
		}
		void inputs(PLAYER &player1, PLAYER &player2, bool &vsIA, Vector2 &ballPosition, bool &pause, GameState &state)
		{
			pauseInputs(pause, state, player1, player2, ballPosition);

			if (pause == false)
			{
				if (player1.upKey >= KEY_ZERO && player1.upKey <= KEY_NINE)
				{
					if (IsKeyDown(player1.upKey + numPadRange) && player1.posy >= 0)
					{
						player1.posy -= player1.speed * GetFrameTime();
					}
				}
				if (player1.downKey >= KEY_ZERO && player1.downKey <= KEY_NINE)
				{
					if (IsKeyDown(player1.downKey + numPadRange) && player1.posy <= screenHeight - player1.padH)
					{
						player1.posy += player1.speed * GetFrameTime();
					}
				}
				if (IsKeyDown(player1.upKey) && player1.posy >= 0)
				{
					player1.posy -= player1.speed * GetFrameTime();
				}
				if (IsKeyDown(player1.downKey) && player1.posy <= screenHeight - player1.padH)
				{
					player1.posy += player1.speed * GetFrameTime();
				}
				if (vsIA == false)
				{
					if (player2.upKey >= KEY_ZERO && player2.upKey <= KEY_NINE)
					{
						if (IsKeyDown(player2.upKey + numPadRange) && player2.posy >= 0)
						{
							player2.posy -= player2.speed * GetFrameTime();
						}
					}
					if (player2.downKey >= KEY_ZERO && player2.downKey <= KEY_NINE)
					{
						if (IsKeyDown(player2.downKey + numPadRange) && player2.posy <= screenHeight - player2.padH)
						{
							player2.posy += player2.speed * GetFrameTime();
						}
					}
					if (IsKeyDown(player2.upKey) && player2.posy >= 0)
					{
						player2.posy -= player2.speed * GetFrameTime();
					}
					if (IsKeyDown(player2.downKey) && player2.posy <= screenHeight - player2.padH)
					{
						player2.posy += player2.speed * GetFrameTime();
					}
				}
				else if (vsIA == true)
				{
					iA(player2, ballPosition);
				}
			}

		}
		void victoryCondition(PLAYER &player1, PLAYER &player2, GameState &state)
		{
			if (player1.points == WIN_POINTS)
			{
				player1.win = true;
				state = WinScreen;
				player1.roundCounter++;
			}
			else if (player2.points == WIN_POINTS)
			{
				player2.win = true;
				state = WinScreen;
				player2.roundCounter++;
			}
		}
		void update(Vector2 &ballPosition, Vector2 &ballSpeed, int ballRadius, PLAYER &player1, PLAYER &player2, bool &collision, GameState &state, bool pause)
		{
			if (pause == false)
			{
				ballBehavior(ballPosition, ballSpeed, ballRadius, player1, player2, collision, screenWidth, screenHeight, middleScreenW, middleScreenH);
				victoryCondition(player1, player2, state);

				if (GetRandomValue(minRandNum, maxRandNum) == powerUpTrigger && activePowerUp == false)
				{
					activePowerUp = true;
					currentPowerUp = (PowerUp)GetRandomValue(Shield, SpeedDown);
					poweredPlayer = GetRandomValue(PLAYER1, PLAYER2);
				}
				if (GetRandomValue(minRandNum, maxTimedRand) == powerUpTrigger && activeTimedPower == false)
				{
					activeTimedPower = true;
					currentTimedPUp = (TimedPUps)GetRandomValue(InvertedControls, ObstacleRec);
					timePoweredPlayer = GetRandomValue(PLAYER1, PLAYER2);
					Obstacle.y = static_cast<float>(GetRandomValue(ObstacleMinY, ObstacleMaxY));
				}
				if (activeTimedPower == true)
				{
					powerTimer -= timeDecreaser * GetFrameTime();

					if (powerTimer <= 0)
					{
						currentTimedPUp = ResetDefValues;
						switch (timePoweredPlayer)
						{
						case PLAYER1:

							timedPowers(player1, ballPosition, ballSpeed, ballRadius);

							break;

						case PLAYER2:

							timedPowers(player2, ballPosition, ballSpeed, ballRadius);

							break;
						}
						powerTimer = defaultPowerTime;
						activeTimedPower = false;
					}

					switch (timePoweredPlayer)
					{
					case PLAYER1:

						timedPowers(player1, ballPosition, ballSpeed, ballRadius);

						break;

					case PLAYER2:

						timedPowers(player2, ballPosition, ballSpeed, ballRadius);

						break;
					}
				}
				if (activePowerUp == true)
				{
					switch (poweredPlayer)
					{
					case PLAYER1:

						powerUps(player1);

						break;

					case PLAYER2:

						powerUps(player2);

						break;
					}
				}
				if (activePowerUp == true)
				{
					textTimer -= timeDecreaser * GetFrameTime();
					if (textTimer < -1)
					{
						textTimer = -1;
					}
				}
				else
				{
					textTimer = defaultTextTime;
				}
			}
		}
		void drawPause(PLAYER player1, PLAYER player2, Vector2 &ballPosition, int ballRadius)
		{
			DrawRectangle(static_cast<int>(player1.posx), static_cast<int>(player1.posy), player1.padW, player1.padH, DARKGRAY);
			DrawRectangle(static_cast<int>(player2.posx), static_cast<int>(player2.posy), player2.padW, player2.padH, DARKGRAY);
			DrawCircleV(ballPosition, static_cast<float>(ballRadius), DARKGRAY);
			DrawText(FormatText("P1 Points: %0i", player1.points), screenZero+70, screenZero+10, minFontSize, player1.padColor);
			DrawText(FormatText("P2 Points: %0i", player2.points), screenWidth-193, screenZero+10, minFontSize, player2.padColor);
			DrawText(FormatText("%0i", player1.roundCounter), middleScreenW-125, screenZero+40, medFontSize, player1.padColor);
			DrawText(FormatText("%0i", player2.roundCounter), middleScreenW+90, screenZero+40, medFontSize, player2.padColor);
			DrawText("Pause", middleScreenW-60, screenZero+80, maxFontSize, WHITE);
			DrawText("M->Go to menu", middleScreenW-110, middleScreenH-55, medFontSize, MAGENTA);
			DrawText("P->Continue", middleScreenW-90, middleScreenH+15, medFontSize, BLUE);
			DrawText("Press ESC to exit", screenWidth-200, screenHeight-30, minFontSize, WHITE);
		}
		void draw(PLAYER player1, PLAYER player2, Vector2 &ballPosition, int &ballRadius, bool pause)
		{
			if (pause == false)
			{
				BeginDrawing();
				ClearBackground(BLACK);
				DrawCircleV(ballPosition, static_cast<float>(ballRadius), MAROON);
				DrawText(FormatText("P1 Points: %0i", player1.points), screenZero+70, screenZero+10, minFontSize, player1.padColor);
				DrawText(FormatText("P2 Points: %0i", player2.points), screenWidth-193, screenZero+10, minFontSize, player2.padColor);
				DrawText(FormatText("%0i", player1.roundCounter), middleScreenW-125, screenZero+40, medFontSize, player1.padColor);
				DrawText(FormatText("%0i", player2.roundCounter), middleScreenW+90, screenZero+40, medFontSize, player2.padColor);
				DrawText("P: Pause", middleScreenW-46, screenZero+10, minFontSize, WHITE);
				DrawRectangle(static_cast<int>(player1.posx), static_cast<int>(player1.posy), player1.padW, player1.padH, player1.padColor);
				DrawRectangle(static_cast<int>(player2.posx), static_cast<int>(player2.posy), player2.padW, player2.padH, player2.padColor);

				if (activeTimedPower == true)
				{
					if (currentTimedPUp == ObstacleRec)
					{
						DrawRectangle(static_cast<int>(Obstacle.x), static_cast<int>(Obstacle.y), static_cast<int>(Obstacle.width), static_cast<int>(Obstacle.height), WHITE);
					}
					else
					{
						DrawText("?", middleScreenW - 8, screenHeight - 70, medFontSize, WHITE);
						DrawText("Inverted controls", middleScreenW - 90, screenHeight - 30, minFontSize, WHITE);
					}
				}

				if (activePowerUp == true && textTimer >= 0)
				{
					switch (poweredPlayer)
					{
					case PLAYER1:

						switch (currentPowerUp)
						{
						case Shield:

							DrawText("P1 Shield", middleScreenW-70, middleScreenH-25, medFontSize, player1.padColor);

							break;

						case SizeUp:

							DrawText("P1 Size Up", middleScreenW-78, middleScreenH - 25, medFontSize, player1.padColor);

							break;

						case SpeedUp:

							DrawText("P1 Speed Up", middleScreenW-95, middleScreenH - 25, medFontSize, player1.padColor);

							break;

						case SpeedDown:

							DrawText("P1 Speed Down", middleScreenW-113, middleScreenH - 25, medFontSize, player1.padColor);

							break;
						}
						break;

					case PLAYER2:

						switch (currentPowerUp)
						{
						case Shield:

							DrawText("P2 Shield", middleScreenW-70, middleScreenH - 25, medFontSize, player2.padColor);

							break;

						case SizeUp:

							DrawText("P2 Size Up", middleScreenW-78, middleScreenH - 25, medFontSize, player2.padColor);

							break;

						case SpeedUp:

							DrawText("P2 Speed Up", middleScreenW-95, middleScreenH - 25, medFontSize, player2.padColor);

							break;

						case SpeedDown:

							DrawText("P2 Speed Down", middleScreenW-113, middleScreenH - 25, medFontSize, player2.padColor);

							break;
						}
						break;
					}
				}
				if (activePowerUp == true && currentPowerUp == Shield && poweredPlayer == PLAYER1)
				{
					DrawRectangle(static_cast<int>(player1.shield.x), static_cast<int>(player1.shield.y), static_cast<int>(player1.shield.width), static_cast<int>(player1.shield.height), WHITE);
				}
				else if (activePowerUp == true && currentPowerUp == Shield && poweredPlayer == PLAYER2)
				{
					DrawRectangle(static_cast<int>(player2.shield.x), static_cast<int>(player2.shield.y), static_cast<int>(player2.shield.width), static_cast<int>(player2.shield.height), WHITE);
				}
			}
			else
			{
				drawPause(player1, player2, ballPosition, ballRadius);
			}
		}
		void deinit()
		{

		}
	}
}