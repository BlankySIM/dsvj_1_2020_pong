#ifndef GAMEPLAY_H 
#define GAMEPLAY_H
#include"raylib.h"
#include "player\player.h"
#include "scenes\screens_states.h"
#include "ball\ball.h"

namespace pong
{
	using namespace player;
	using namespace ball;

	namespace gameplay
	{
		void init();
		void iA(PLAYER &player2, Vector2 ballposition);
		void inputs(PLAYER &player1, PLAYER &player2, bool &vsIA, Vector2 &ballPosition, bool &pause, GameState &state);
		void pauseInputs(bool &pause, GameState &state, PLAYER &player1, PLAYER &player2, Vector2 &ballPosition);
		void victoryCondition(PLAYER &player1, PLAYER &player2, GameState &state);
		void update(Vector2 &ballPosition, Vector2 &ballSpeed, int ballRadius, PLAYER &player1, PLAYER &player2, bool &collision, GameState &state, bool pause);
		void drawPause(PLAYER player1, PLAYER player2, Vector2 &ballPosition, int ballRadius);
		void draw(PLAYER player1, PLAYER player2, Vector2 &ballPosition, int &ballRadius, bool pause);
		void deinit();
	}
}
#endif