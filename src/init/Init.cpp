#include "init.h"

namespace pong
{
	float player::p1DefaultX = screenZero+25;
	float player::p1DefaultY = middleScreenH-70;
	float player::p2DefaultX = screenWidth-50;
	float player::p2DefaultY = middleScreenH-70;
	const int selectedColorDefault = 7;
	const int playersDefSpeed = 200;
	const int shieldW = 7;
	bool collision = false;
	bool pause = false;
	player::PLAYER player1 = {p1DefaultX, p1DefaultY, padW, padH, 0, false, 0, SKYBLUE, P1Up, P1Down, playersDefSpeed, false, screenZero, screenZero, shieldW, screenHeight };
	player::PLAYER player2 = {p2DefaultX, p2DefaultY, padW, padH, 0, false, 0, RED, P2Up, P2Down, playersDefSpeed, false, screenWidth - shieldW, screenZero, shieldW, screenHeight };
	GameState state = MenuScreen;
	Vector2 ballPosition = { middleScreenW, middleScreenH };
	Vector2 ballSpeed = { 400.0f, 300.0f };
	int ballRadius = 20;
	Color PadColorList[7] = { RED, ORANGE, YELLOW, GREEN, SKYBLUE, VIOLET, PINK };
	bool vsIA = false;
	int colorPointer = 0;
	int selectedColor = selectedColorDefault;
}