#ifndef INIT_H
#define INIT_H
#include "raylib.h"
#include "player\player.h"
#include "scenes\screens_states.h"

namespace pong
{
	using namespace player;

	extern const int playersDefSpeed;
	extern bool collision;
	extern bool pause;
	extern PLAYER player1;
	extern PLAYER player2;
	extern GameState state;
	extern Vector2 ballPosition;
	extern Vector2 ballSpeed;
	extern int ballRadius;
	extern Color PadColorList[7];
	extern bool vsIA;
	extern int colorPointer;
	extern int selectedColor;
}
#endif