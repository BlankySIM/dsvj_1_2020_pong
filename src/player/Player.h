#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"

namespace pong
{
	namespace player
	{
		extern float p1DefaultX;
		extern float p1DefaultY;
		extern float p2DefaultX;
		extern float p2DefaultY;
		extern const int padW;
		extern const int padH;
		extern const int P1Up;
		extern const int P1Down;
		extern const int P2Up;
		extern const int P2Down;

		struct PLAYER
		{
			float posx;
			float posy;
			int padW;
			int padH;
			int points;
			bool win;
			int roundCounter;
			Color padColor;
			int upKey;
			int downKey;
			int speed;
			bool activeShield;
			Rectangle shield;
		};
	}
}
#endif