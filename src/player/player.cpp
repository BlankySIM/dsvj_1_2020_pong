#include "player.h"

namespace pong
{
	namespace player
	{
		const int padW = 23;
		const int padH = 150;
		const int P1Up = KEY_Q;
		const int P1Down = KEY_A;
		const int P2Up = KEY_I;
		const int P2Down = KEY_K;
	}
}