#ifndef BALL_H
#define BALL_H
#include"raylib.h"
#include "player/player.h"
#include "powerups/powerups.h"

namespace pong
{
	using namespace player;
	using namespace powerups;

	namespace ball
	{
		extern bool activePowerUp;
		extern const float defaultPowerTime;
		extern float powerTimer;
		extern float timeDecreaser;

		void ballBehavior(Vector2 &ballPosition, Vector2 &ballSpeed, int ballRadius, PLAYER &player1, PLAYER &player2, bool &collision, float screenWith, float screenHeigh, float middleScreenW, float middleScreenH);
	}
}
#endif

