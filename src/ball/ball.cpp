#include "ball.h"

namespace pong
{
	namespace ball
	{
		const int COLLIDER_RANGE = 100;
		bool activePowerUp = false;
		const float defaultPowerTime = 200;
		float powerTimer = defaultPowerTime;
		float timeDecreaser = 30;

		void ballBehavior(Vector2 &ballPosition, Vector2 &ballSpeed, int ballRadius, PLAYER &player1, PLAYER &player2, bool &collision, float screenWith, float screenHeigh, float middleScreenW, float middleScreenH)
		{
			ballPosition.x += ballSpeed.x * GetFrameTime();
			ballPosition.y += ballSpeed.y * GetFrameTime();

			if ((ballPosition.y >= (screenHeigh - ballRadius)) || (ballPosition.y <= ballRadius))
			{
				ballSpeed.y *= -1.0f;
			}

			if (collision == false)
			{
				if (ballPosition.x <= player1.posx + player1.padW && ballPosition.y + ballRadius < player1.posy + COLLIDER_RANGE && ballPosition.y + ballRadius >= player1.posy
					|| ballPosition.x >= player2.posx && ballPosition.y + ballRadius < player2.posy + COLLIDER_RANGE && ballPosition.y + ballRadius >= player2.posy
					|| ballPosition.x <= player1.posx + player1.padW && ballPosition.y - ballRadius > player1.posy + player1.padH - COLLIDER_RANGE && ballPosition.y - ballRadius <= player1.posy + player1.padH
					|| ballPosition.x >= player2.posx && ballPosition.y - ballRadius > player2.posy + player2.padH - COLLIDER_RANGE && ballPosition.y - ballRadius <= player2.posy + player2.padH)
				{
					ballSpeed.y *= -1.0f;
					collision = true;
				}
				else if (ballPosition.x - ballRadius <= player1.posx + player1.padW && ballPosition.y >= player1.posy && ballPosition.y <= player1.posy + player1.padH
					|| ballPosition.x + ballRadius >= player2.posx && ballPosition.y >= player2.posy && ballPosition.y <= player2.posy + player2.padH)
				{
					ballSpeed.x *= -1.0f;
					collision = true;
				}
			}

			if (ballPosition.x >= middleScreenW - COLLIDER_RANGE && ballPosition.x <= middleScreenW + COLLIDER_RANGE)
			{
				collision = false;
			}

			if (ballPosition.x <= 0 + ballRadius)
			{
				if (player1.activeShield == false)
				{
					ballPosition = { middleScreenW, middleScreenH };
					player2.points++;
					ballSpeed.x *= -1.0f;
					currentPowerUp = None;
					powerUps(player1);
					powerUps(player2);
					activePowerUp = false;
					powerTimer = 0;
				}
				else
				{
					ballSpeed.x *= -1.0f;
					collision = true;
					currentPowerUp = None;
					powerUps(player1);
					activePowerUp = false;
				}
			}
			if (ballPosition.x >= screenWith - ballRadius)
			{
				if (player2.activeShield == false)
				{
					ballPosition = { middleScreenW, middleScreenH };
					ballSpeed.x *= -1.0f;
					player1.points++;
					currentPowerUp = None;
					powerUps(player1);
					powerUps(player2);
					activePowerUp = false;
					powerTimer = 0;
				}
				else
				{
					ballSpeed.x *= -1.0f;
					collision = true;
					currentPowerUp = None;
					powerUps(player2);
					activePowerUp = false;
				}
			}
		}
	}
}