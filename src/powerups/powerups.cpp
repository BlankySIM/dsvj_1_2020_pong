#include "powerups.h"

namespace pong
{
	namespace powerups
	{
		const int initialPosition = 0;
		const int playersDefSpeed = 200;
		const int incrementedPlayersSpeed = 280;
		const int decreasedPlayersSpeed = 100;
		const int playersIncrementedH = 200;
		const int halfPadY = padH / 2;
		const int ObstacleX = 397;
		const int ObstacleW = 4;
		bool obstacleCollisionFlag = false;
		bool invertedControlsFlag = false;
		int auxKey = 0;
		PowerUp currentPowerUp = None;
		TimedPUps currentTimedPUp = ResetDefValues;
		Rectangle Obstacle = { static_cast<float>(ObstacleX), static_cast<float>(initialPosition), static_cast<float>(ObstacleW), static_cast<float>(halfPadY) };

		void powerUps(PLAYER &player)
		{
			switch (currentPowerUp)
			{
			case None:

				player.activeShield = false;
				player.speed = playersDefSpeed;
				player.padH = padH;

				break;

			case Shield:

				player.activeShield = true;

				break;

			case SizeUp:

				player.padH = playersIncrementedH;

				break;

			case SpeedUp:

				player.speed = incrementedPlayersSpeed;

				break;

			case SpeedDown:

				player.speed = decreasedPlayersSpeed;

				break;
			}
		}
		void timedPowers(PLAYER &player, Vector2 &ballPosition, Vector2 &ballSpeed, int ballRadius)
		{
			switch (currentTimedPUp)
			{
			case ResetDefValues:

				if (invertedControlsFlag == true)
				{
					auxKey = player.upKey;
					player.upKey = player.downKey;
					player.downKey = auxKey;
					invertedControlsFlag = false;
				}

				break;

			case InvertedControls:

				if (invertedControlsFlag == false)
				{
					auxKey = player.upKey;
					player.upKey = player.downKey;
					player.downKey = auxKey;
					invertedControlsFlag = true;
				}

				break;

			case ObstacleRec:

				if (CheckCollisionCircleRec(ballPosition, static_cast<float>(ballRadius), Obstacle) && obstacleCollisionFlag == false)
				{
					ballSpeed.x *= -1.0f;
					obstacleCollisionFlag = true;
				}
				if (CheckCollisionCircleRec(ballPosition, static_cast<float>(ballRadius), Obstacle) == false)
				{
					obstacleCollisionFlag = false;
				}

				break;
			}
		}
	}
}