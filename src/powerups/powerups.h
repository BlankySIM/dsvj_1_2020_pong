#ifndef POWERUPS_H
#define POWERUPS_H
#include "raylib.h"
#include "player/player.h"

namespace pong
{
	using namespace player;

	namespace powerups
	{
		enum PowerUp
		{
			None,
			Shield,
			SizeUp,
			SpeedUp,
			SpeedDown
		};

		enum TimedPUps
		{
			ResetDefValues,
			InvertedControls,
			ObstacleRec
		};

		extern const int playersDefSpeed;
		extern const int halfPadY;
		extern PowerUp currentPowerUp;
		extern TimedPUps currentTimedPUp;
		extern Rectangle Obstacle;

		void powerUps(PLAYER &player);
		void timedPowers(PLAYER &player, Vector2 &ballPosition, Vector2 &ballSpeed, int ballRadius);
	}
}
#endif

